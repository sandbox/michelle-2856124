The Lingotek Files module allows translating file entities along with the
parent nodes. It is based on the Lingotek Pods sandbox module:

https://www.drupal.org/sandbox/jtsnow/2454315
